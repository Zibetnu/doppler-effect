extends Node2D


export(NodePath) onready var entity_path
export var entity_label = ""
var EntityNode = null


func _ready():
	if not entity_path: queue_free()
	EntityNode = get_node(entity_path)
	$NameLabel.text = entity_label
	$XSlider.value = EntityNode.velocity_x
	$YSlider.value = EntityNode.velocity_y
	$SourceCheckBox.pressed = EntityNode.source
	$ListenerCheckBox.pressed = EntityNode.listener


func _on_XSlider_value_changed(value):
	EntityNode.velocity_x = $XSlider.value
	$XLabel.text = str($XSlider.value)


func _on_YSlider_value_changed(value):
	EntityNode.velocity_y = $YSlider.value
	$YLabel.text = str($YSlider.value)


func _on_SourceCheckBox_toggled(button_pressed):
	EntityNode.set_as_source(button_pressed)


func _on_ListenerCheckBox_toggled(button_pressed):
	EntityNode.listener = button_pressed
