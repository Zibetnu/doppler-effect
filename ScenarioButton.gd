extends Button


export var parameters = {}


func _on_ScenarioButton_pressed():
	if get_node("%AnimationPlayer").is_playing():
		get_node("%AnimationPlayer").play("RESET")
		
	for sound_wave in get_tree().get_nodes_in_group("sound_waves"):
		sound_wave.queue_free()

	var entities = [get_node("%Entity"), get_node("%Entity2")]
	var controls = [get_node("%EntityControls"), get_node("%EntityControls2")]
	for Entity in entities:
		if Entity.name + "_x" in parameters.keys():
			Entity.position.x = parameters[Entity.name + "_x"]

		if Entity.name + "_vy" in parameters.keys():
			Entity.position.y = parameters[Entity.name + "_y"]

		if Entity.name + "_vx" in parameters.keys():
			Entity.velocity_x = parameters[Entity.name + "_vx"]

		if Entity.name + "_vy" in parameters.keys():
			Entity.velocity_y = parameters[Entity.name + "_vy"]

		if Entity.name + "_s" in parameters.keys():
			Entity.set_as_source(parameters[Entity.name + "_s"])

		if Entity.name + "_l" in parameters.keys():
			Entity.listener = parameters[Entity.name + "_l"]

	if "run_anim" in parameters.keys():
		get_node("%AnimationPlayer").play("Senario4")

	for control in controls:
		control._ready()
