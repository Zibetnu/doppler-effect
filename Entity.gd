extends Node2D


export var listener = false
export var velocity_x = 0
export var velocity_y = 0
var source = false

onready var SoundWave = preload("res://SoundWave.tscn")


func _ready():
	set_as_source(source)


func _physics_process(delta):
	self.position += Vector2(velocity_x, velocity_y * -1)


func set_as_source(make_source):
	source = make_source
	if not make_source:
		$Timer.stop()
		return

	$Timer.start()


func set_draggable(draggable:bool):
	$Area2D.draggable = draggable


func _on_Timer_timeout():
	var Instance = SoundWave.instance()
	Instance.global_position = global_position
	Instance.origin = self.name
	Instance.velocity_x = velocity_x
	Instance.velocity_y = velocity_y
	get_parent().add_child(Instance)


func _on_Area2D_area_entered(area):
	if listener and area.get_parent().origin != self.name:
		area.get_parent().get_node("AudioStreamPlayer2D").play()
