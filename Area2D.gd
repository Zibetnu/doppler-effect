extends Area2D


var draggable = true
var should_wait = false # Prevents spamming.


func _ready():
	set_process(false)


func _process(delta):
	get_parent().global_position = get_global_mouse_position()


func _on_Area2D_input_event(viewport, event, shape_idx):
	if not draggable: return
	if should_wait: return
	should_wait = true

	if ( # Starts moving if conditions are right.
			Input.is_action_just_pressed("select")
			and not(is_processing())
	):
		set_process(true)

	elif ( # Stops moving if conditions are right.
			Input.is_action_just_pressed("select") or Input.is_action_just_released("select")
			and is_processing()
	):
		set_process(false)

	yield(get_tree(), "idle_frame")
	should_wait = false
