A small showcase of the Doppler effect that I quickly slapped together with Godot. Visit https://zibetnu.gitlab.io/doppler-effect/ to try it out!
