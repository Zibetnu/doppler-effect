extends Node2D


var origin = ""
var played = false
export var velocity_x = 0
export var velocity_y = 0


func _ready():
	$AnimationPlayer.play("Fade")


func _physics_process(delta):
	scale += Vector2(0.05, 0.05)
	position += Vector2(
		clamp(velocity_x, -1, 1), 
		clamp(velocity_y, -1, 1) * -1
	)


func _on_Timer_timeout():
	queue_free()
